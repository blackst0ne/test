<?php
namespace RadekDostal\Kotliky\Models;

use Dibi\Exception;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\Passwords;
use RadekDostal\Kotliky\Exceptions\FailedDataException;

class Authenticator extends BaseModel implements IAuthenticator
{
  public function authenticate(array $credentials)
  {
    try
    {
      $user = $this->connection->select([
        'user_id',
        'passwd'
      ])
        ->from('user')
        ->where('[e_mail] = %s', $credentials[self::USERNAME])
        ->and('[registration_verified] = %i', 1)
        ->fetch();
    }
    catch (Exception $e)
    {
      $this->logMessage($e, __METHOD__);
      throw new FailedDataException($e->getMessage());
    }

    if ($user === FALSE || Passwords::verify($credentials[self::PASSWORD], $user->offsetGet('passwd')) === FALSE)
      throw new AuthenticationException('E-mail nebyl nenalezen nebo bylo zadáno neplatné heslo.', self::FAILURE);

    $profile = [
      'id' => $user->offsetGet('user_id')
    ];

    return something;
  }
}